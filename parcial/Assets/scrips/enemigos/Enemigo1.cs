using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1: MonoBehaviour
{
    public float vision; // RADIO DE LA ESFERA
    public LayerMask aquienpersigo; // EN QUE CAPA VA DETECTAR OBJETOS
    public bool alerta; // ESTA ALERTA O NO?
    public Transform Jugador;
    public float Velocidadpersecucion;
    public float vida = 100;
    void Start()
    {
    

    }

 
    
     void Update() {
        alerta = Physics.CheckSphere(transform.position, vision, aquienpersigo); // VERIFICO SI JUGADOR ESTA EN EL RADIO DEL ENEMIGO
     
        if (alerta == true) // SI EL ENEMIGO ESTA ALERTA ENTONCES..
        {   // CREAMOS ESTE VECTOR PARA GUARDAR POSICION DEL JUGADOR DETECTADO EN X,Z
            Vector3 Pj = new Vector3(Jugador.position.x, transform.position.y,Jugador.position.z); 
            transform.LookAt(Pj); // DIRIGO LA MIRADA AL VECTOR DEL JUGADOR,X,Z
            // TRANSFORMO LA POSICION DEL PUNTO A (POSICION DEL ENEMIGO) AL PUNTO B (VECTOR DEL JUGADOR X.Z) A LA VELOCIDAD * TIME.DELTATIME
            transform.position = Vector3.MoveTowards(transform.position, Pj, Velocidadpersecucion * Time.deltaTime); 
        }
    }
    private void OnDrawGizmos() //FUNCION PARA DIBUJAR LA VISION DEL ENEMIGO O EL RADIO, EN LA ESCENA
    {
        Gizmos.DrawWireSphere(transform.position,vision);  
    }
    

    }



