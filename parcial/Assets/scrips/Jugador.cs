using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;

public class Jugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    public float salto;
    public float maxsalto;
    public float contador;
    public TMPro.TMP_Text tiemporestante;
    public TMPro.TMP_Text vida;
    public TMPro.TMP_Text velocidad;
    public TMPro.TMP_Text ganaste;
    public TMPro.TMP_Text gameover;
    public TMPro.TMP_Text buscabola;
    public float vidajugador = 100;
    public bool caida;
    public LayerMask suelo;
    public float poderenemigo5 = 20;
    public float fichagandora = 0;
    public float tiempo = 10;




    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        vida.text = " ";
        velocidad.text = " ";
        tiemporestante.text = " ";
        ganaste.text = " ";
        gameover.text = " ";
        buscabola.text = "";
        setearTextos();
    }
    private void setearTextos()
    {
        velocidad.text = "velocidad:" + rapidez.ToString();
        vida.text = "vida:" + vidajugador.ToString();
        tiemporestante.text = "tiempo restante:" + tiempo.ToString("f0");
        buscabola.text = "busca la bola de fuego";


        
    }
    
    void Update()
    {
        setearTextos();

        tiempo = tiempo - Time.deltaTime;
        
        if (vidajugador < 1 || tiempo <= 0)
        {
            gameover.text = "gameover";
            Time.timeScale = 0f;
        }
       
        else if (vidajugador >= 2 || tiempo >= 1)
        {
            gameover.text = " ";
        }
        if (EstaEnPiso())
        {
            salto = 0;

        } // SI JUGADOR ESTA EN EL SUELO ENTONCES EL CONTADOR DE SALTOS VUELVE A 0

        if (Input.GetKeyDown(KeyCode.Space) && salto <= maxsalto)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            salto++;
        } // SI APRETO ESPACIO Y SALTO ES MENOS O IGUAL A MAX SALTO ENTONCES PUEDO SALTAR

        if (Input.GetKeyDown(KeyCode.R))

        {   // reiniciamos juego
            tiempo = 60;
            reiniciarjuego();
        } // REINICIAR JUGADOR CON BOTON R
        if (fichagandora == 1) { 
            ganaste.text = "Ganaste!";
            setearTextos();
            Time.timeScale = 0f;
        }
        else if (fichagandora == 0) { ganaste.text = " "; }



    }
    public void reiniciarjuego()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal"); float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    } // VERIFICA SI MI PERSONAJE CAPSULA ESTA TOCANDO EL SUELO.
    private void OnTriggerEnter(Collider other) // DETECTA SI EL OBJETO QUE CONTIENE ESTE SCRIP LE ENTRA UNA COLISION DE UN OBJETO TIPO TRIGER
    {   //CONSULTAMOS SI COLISIONO UN TAG ESPECIFICO
        if (other.CompareTag("Enemigo") == true) // SI SE CUMPLE LA SENTENCIA ENTONCES DISPARAMOS EVENTO
        {
            vidajugador = vidajugador - 30;

            other.gameObject.SetActive(false);
            Vector3 escala = new Vector3(0, 10, 0);
            transform.position = transform.position + escala;
            setearTextos();
        }
        if (other.gameObject.CompareTag("coleccionable") == true)

        {
            other.gameObject.SetActive(false);
            rapidez = rapidez + 5;

            Vector3 escala = transform.localScale;
            transform.localScale = escala * 2;
            setearTextos();
        }
        if (other.gameObject.CompareTag("+vida") == true)

        {
            other.gameObject.SetActive(false);
            vidajugador = vidajugador + 50;
            setearTextos();
        }
        if (other.gameObject.CompareTag("zanja"))

        {   // reiniciamos jugador
            tiempo = 60;
            reiniciarjuego();
            setearTextos();
        }
        if (other.gameObject.CompareTag("Enemigo2"))

        {
            Vector3 posicionj = new Vector3(8, 0, 8);
            transform.position = transform.position - posicionj * Time.deltaTime;
            vidajugador = vidajugador - 30;
            setearTextos();
        }
        if (other.gameObject.CompareTag("Enemigo3"))

        {
            vidajugador = vidajugador - 20;
            Vector3 posicionj = new Vector3(5, 0, 5);
            transform.position = transform.position - posicionj * Time.deltaTime;
            setearTextos();
        }
        if (other.gameObject.CompareTag("Enemigo4"))

        {
            vidajugador = vidajugador - 20;
            Vector3 posicionj = new Vector3(8, 8, 8);
            transform.position = transform.position - posicionj * Time.deltaTime;
            setearTextos();
        }
        if (other.gameObject.CompareTag("Enemigo5"))

        {
            poderenemigo5 = poderenemigo5 + 5;
            vidajugador = vidajugador - poderenemigo5;
            Vector3 posicionj = new Vector3(8, 8, 8);
            transform.position = transform.position - posicionj * Time.deltaTime;
            setearTextos();
        }
        if (other.gameObject.CompareTag("ganaste"))

        {
            fichagandora = 1;
            setearTextos();
        }
    }

}
